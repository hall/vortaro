var url = "https://ido.bryton.io"
var response;
var query;
function search(event) {
    if(query == "") {return;}
    if(event.key === 'Enter' || event == true) {
        var xmlhttp = new XMLHttpRequest();
        loader(true);
	query = document.getElementById('demando').value;

        if (match = /^([a-z]{2,3}):/i.exec(query)) {
            language = match[1].toLowerCase();
            query = query.replace(match[1]+':', '');
        }

	linguo = document.getElementById('lingui').value;

        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                loader(false);

                var definitions = document.getElementById('definitions');
		while (definitions.hasChildNodes()) {
    			definitions.removeChild(definitions.lastChild);
		}

                response = JSON.parse(this.responseText);

		var vorto = document.createElement("dt");
		vorto.innerHTML = '<b>'+response[query].io+'</b>' + '<span class="right">'+response[query][linguo]+'</span>';
		definitions.appendChild(vorto);

		var semantiki = response[query].semantiko;

		if (Array.isArray(semantiki)) {
			for (var i=0; i<semantiki.length; i++) {
				var semantiko = document.createElement("dd");
				semantiko.innerHTML = response[query].semantiko[i];
				semantiko.ondblclick = edit;
				definitions.appendChild(semantiko);
			}
		} else {
			var semantiko = document.createElement("dd");
			semantiko.innerHTML = response[query].semantiko;
			semantiko.ondblclick = edit;
			definitions.appendChild(semantiko);
		}

		var exemplaro = document.createElement("div");
		exemplaro.innerHTML = response[query].exemplaro;
		exemplaro.ondblclick = edit;

		definitions.appendChild(exemplaro);

            }
        };
        if (tradukuro.value) {
            xmlhttp.open("GET", url + "/" + query + "/" + linguo + ",io,semantiko,exemplaro,sinonimo,antonimo,kompundi", true);
        } else {
            xmlhttp.open("GET", url + "/" + linguo + ":" + query + "/" + linguo + ",io,semantiko,exemplaro,sinonimo,antonimo,kompundi", true);
        }
        xmlhttp.send();
    }
}


function edit() {
    if (seruro.value) { return }
    var input = document.createElement("input");
    input.type = 'text';
    input.value = this.innerHTML;
    input.style = 'padding:0;margin:0;';

    this.innerHTML = '';
    this.appendChild(input);
    var parent = input.parentElement;
    input.focus();

    input.onblur = save;
}

function save() {
    this.parentElement.innerHTML = this.value; this.parentElement.removeChild(input)

    var putreq = new XMLHttpRequest();
    loader(true);

    //linguo = document.getElementById('lingui').value;

    var vorto = response[query].io.replace('.','');
    putreq.open("PUT", url + "/" + vorto+"/"+this.id, true);
    putreq.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    putreq.onload = function() {
        if (this.readyState == 4 && this.status == 200) {
            loader(false);

            var response = JSON.parse(this.responseText);
        }
    };
    putreq.send(encodeURI("nova="+this.innerHTML));
}

function loader(state) {
    var demando = document.getElementById('demando');

    if (state) {
        demando.style.backgroundImage = "url(\"data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' width='25' height='25' viewBox='0 0 50 50' opacity='0.7' style='enable-background:new 0 0 50 50;' xml:space='preserve'> <path d='M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z'> <animateTransform attributeType='xml' attributeName='transform' type='rotate' from='0 25 25' to='360 25 25' dur='0.6s' repeatCount='indefinite'/></path></svg>\")";
    } else {
        demando.style.backgroundImage = "url(\"data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 48 48' width='32' height='32' fill='none' stroke='currentcolor' opacity='0.7' stroke-linecap='round' stroke-linejoin='round' stroke-width='3'><circle cx='14' cy='14' r='12' /><path d='M23 23 L30 30'  /></svg>\")";
    }
}

var tradukuro = document.getElementById('tradukuro');
function translateToLang() {

    if (tradukuro.value) {
        tradukuro.style.backgroundImage = "url(\"data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 48 48' width='24' height='24' fill='none' stroke='currentcolor' stroke-linecap='round' stroke-linejoin='round' stroke-width='4'><path d='M10 6 L2 16 10 26 M2 16 L30 16' /></svg>\")";
        tradukuro.value = false;
    } else {
        tradukuro.style.backgroundImage = "url(\"data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 48 48' width='24' height='24' fill='none' stroke='currentcolor' stroke-linecap='round' stroke-linejoin='round' stroke-width='4'><path d='M22 6 L30 16 22 26 M30 16 L2 16' /></svg>\")";
        tradukuro.value = true;
    }
}

var seruro= document.getElementById('seruro');
function editable() {

    if (seruro.value) {
        seruro.style.backgroundImage = "url(\"data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32' width='24' height='24' fill='none' stroke='currentcolor' stroke-linecap='round' stroke-linejoin='round' stroke-width='3'><path d='M5 15 L5 30 27 30 27 15 Z M9 15 C9 7 9 3 16 3 23 3 23 8 23 9 M16 20 L16 23' /><circle cx='16' cy='24' r='1' /></svg>\")";
	seruro.value = false;
    } else {
        seruro.style.backgroundImage = "url(\"data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32' width='24' height='24' fill='none' stroke='currentcolor' stroke-linecap='round' stroke-linejoin='round' stroke-width='3'><path d='M5 15 L5 30 27 30 27 15 Z M9 15 C9 9 9 5 16 5 23 5 23 9 23 15 M16 20 L16 23' /><circle cx='16' cy='24' r='1' /></svg>\")";
	seruro.value = true;
    }
}

window.onload = function() {
    var userlang = (window.navigator.userLanguage || window.navigator.language).replace(/-.*/g, '');
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var response = JSON.parse(this.responseText);
            var lingui = document.getElementById('lingui');
            lingui.onchange = function() {search(true)};

            for (var i=0; i<response.length; i++) {
		// skip all non-language entries
		if (response[i].length > 3) { continue; }

		var opt = document.createElement("option");
		opt.innerHTML = response[i];
                lingui.appendChild(opt);
            }
	    lingui.value = userlang;
        }
    };
    xhr.open("GET", url + "/*", true);
    xhr.send();

    loader(false);
    translateToLang();
    editable();

    tradukuro.onclick = function() { translateToLang() };
    seruro.onclick    = function() { editable() };

    document.getElementById("demando").focus();
}
